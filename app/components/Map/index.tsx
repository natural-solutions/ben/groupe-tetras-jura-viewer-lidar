import { FC, useEffect, useState, useMemo } from 'react'
import { useLocalStorage } from 'react-use'
import DeckGL from '@deck.gl/react'
import { ViewStateProps } from '@deck.gl/core/lib/deck'
import Parcelles from '../layers/Parcelles'
import Image from 'next/dist/client/image'
import modelLayer from '../layers/Modele'
import OptionsPanel, { initModel, AUC } from '../OptionsPanel'
import VarsPanel from '../VarsPanel'
import ZoneProtegees from '../layers/ZonesProtegees'
import Massifs from '../layers/Massifs'
import Satellite, { orthophotos, planignv2 } from '../layers/Satellite'
import { GeoJsonLayerProps } from '@deck.gl/layers'
import MapControls, { Action } from './Controls'
import { useTour } from '@reactour/tour'
import bbox from '@turf/bbox'
import { FlyToInterpolator } from '@deck.gl/core'
import { ScaleControl, StaticMap } from 'react-map-gl'

type LayerVisibility = Record<string, boolean>

interface Parcelle {
  commune: string
  prefixe: string
  section: string
  numero: string
  contenance: string
}

interface ParcelleSelected {
  value: string
  label: string
}

function getTooltip({ object }: { object: any }) {
  if (!object) {
    return null
  }

  if (!object.id) return null

  return {
    html: `<div>${object.id}</div>`,
  }
}

const viewportCenter = [6.170291603252868, 46.56777086303595]

const defaultLayerVisiblity = {
  ...Object.keys(AUC).reduce<LayerVisibility>(
    (acc, k) => {
      acc[k] = false
      return acc
    },
    {
      osm: true,
      satellite: false,
    }
  ),
  zonesProtegees: false,
  massifs: false,
  parcelles: true,
  models: true,
}

const defaultLayers: Record<string, any> = {
  parcelles: Parcelles({}),
  massifs: Massifs(false),
  zonesProtegees: ZoneProtegees(false),
  ...Object.keys(AUC).reduce<Record<string, any>>((acc, k) => {
    acc[k] = modelLayer(k, false)
    return acc
  }, {}),
  satellite: Satellite(false),
}

const defaultViewport = {
  zoom: 10,
  longitude: viewportCenter[0],
  latitude: viewportCenter[1],
}

const Map: FC = () => {
  const [viewport, setViewport] = useLocalStorage<ViewStateProps>(
    'viewport',
    defaultViewport
  )
  const [viewstate, setViewstate] = useState<ViewStateProps>(
    viewport || defaultViewport
  )
  const [model, setModel] = useState(initModel)
  const [layersVisibility, setLayersVisibility] =
    useLocalStorage<LayerVisibility>('layersVisibility', defaultLayerVisiblity)
  const [selectedParcelles, setSelectedParcelles] = useState<
    ParcelleSelected[]
  >([])
  const [selectedModel, setSelectedModel] = useState(Object.keys(AUC)[0])
  const [layers, setLayers] = useState(Object.values(defaultLayers))
  const { isOpen, setIsOpen } = useTour()

  const fitToBounds = async () => {
    const massifsLayer = layers.find((l) => l.id === 'massifs')
    const bboxLayer = bbox(massifsLayer.props.data)
    const newViewState = massifsLayer.context.viewport.fitBounds(
      [
        [bboxLayer[0], bboxLayer[1]],
        [bboxLayer[2], bboxLayer[3]],
      ],
      {
        padding: 20,
      }
    )

    setViewstate({
      ...newViewState,
      transitionDuration: 500,
      transitionInterpolator: new FlyToInterpolator(),
    })
  }

  const handleActions = async (action: Action) => {
    const { zoom } = viewstate

    switch (action) {
      case 'zoomIn':
        if (zoom) {
          setViewport({ ...viewport, zoom: zoom + 1 })
          setViewstate({ ...viewstate, zoom: zoom + 1 })
        }
        break
      case 'zoomOut':
        if (zoom) {
          setViewport({ ...viewport, zoom: zoom - 1 })
          setViewstate({ ...viewstate, zoom: zoom - 1 })
        }
        break
      case 'fitBounds':
        fitToBounds()
        break
      case 'help':
        await setModel(initModel)
        setIsOpen(true)
        break
    }
  }

  useEffect(() => {
    let newKey = selectedModel

    if (model.espece === 'GEL') {
      newKey = `modeles_${model.echelle}ha_GEL_${model.departement}`
    } else if (model.saison === 'summer') {
      newKey = `modeles_${model.echelle}ha_GT_summer_${model.departement}`
    } else {
      newKey = `modeles_${model.echelle}ha_GT_${model.sexe}_winter_${model.departement}`
    }

    let newLayersVisibility = { ...layersVisibility }

    for (let key of Object.keys(AUC)) {
      newLayersVisibility[key] = false
    }

    if (model.display) {
      newLayersVisibility[newKey] = true
    }
    setLayersVisibility(newLayersVisibility)
    setSelectedModel(newKey)
    setSelectedParcelles(
      isOpen ? [{ value: '014190000A0001', label: '014190000A0001' }] : []
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [model, isOpen])

  useEffect(() => {
    const newLayers: Record<string, any> = {}

    for (let layerKey in defaultLayerVisiblity) {
      if (layerKey === 'satellite' && layersVisibility) {
        newLayers[layerKey] = layers
          .find(({ id }) => id === layerKey)
          .clone({
            data: layersVisibility[layerKey] ? orthophotos : planignv2,
          })
      } else if (
        layersVisibility &&
        Object.keys(layersVisibility).includes(layerKey) &&
        defaultLayers[layerKey]
      ) {
        const newLayerProps = {
          visible: layersVisibility[layerKey],
        } as GeoJsonLayerProps<any>

        if (layerKey === 'parcelles') {
          newLayerProps.onClick = (e: {
            object: { id: string; properties: Parcelle }
          }) => {
            if (e?.object?.id) {
              const match = selectedParcelles.filter(
                ({ value }) => value === e.object.id
              )

              if (match.length) {
                return setSelectedParcelles(
                  selectedParcelles.filter(({ value }) => value !== e.object.id)
                )
              }

              const parcelle = {
                value: e.object.id,
                label: e.object.id,
              }

              setSelectedParcelles((prevSelectedPArcelles) => [
                ...prevSelectedPArcelles,
                parcelle,
              ])
            }
          }

          newLayerProps.getFillColor = (e) => {
            if (e?.id) {
              const match = selectedParcelles.filter(
                ({ value }) => value === e.id
              )

              if (match.length) {
                return [255, 0, 0]
              }
            }

            return [0, 0, 0, 0]
          }

          newLayerProps.updateTriggers = {
            getFillColor: [selectedParcelles],
          }
        }

        newLayers[layerKey] = defaultLayers[layerKey].clone(newLayerProps)
      }
    }

    setLayers(Object.values(newLayers))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layersVisibility, selectedParcelles])

  return (
    <>
      <DeckGL
        width={'100%'}
        height={'100%'}
        controller={true}
        style={{ overflow: 'hidden' }}
        layers={layers}
        viewState={viewstate}
        getTooltip={getTooltip}
        onAfterRender={() => {
          if (!document) return

          const viewDefaultView = document.getElementById('view-default-view')

          if (viewDefaultView !== null && viewDefaultView.children[0]) {
            const child = viewDefaultView.children[0] as HTMLElement

            child.style.zIndex = 'inherit'
          }
        }}
        onViewStateChange={(viewState) => {
          const { zoom, longitude, latitude } = viewState.viewState
          setViewport({ zoom, longitude, latitude })
          setViewstate(viewState.viewState)
        }}
      >
        <StaticMap
          style={{ width: '100%', height: '100%' }}
          mapOptions={{ hash: true }}
        >
          <ScaleControl
            maxWidth={100}
            unit="metric"
            style={{
              left: 54,
              bottom: 2,
            }}
          />
        </StaticMap>
        <a
          href="https://www.ign.fr/"
          style={{ position: 'absolute', bottom: -3, left: 0 }}
        >
          <Image
            src="https://wxs.ign.fr/static/logos/IGN/IGN.gif"
            width="50px"
            height="50px"
            alt="ign logo"
          />
        </a>
      </DeckGL>

      {useMemo(
        () =>
          selectedParcelles.length > 0 && (
            <VarsPanel
              echelle={model.echelle}
              departement={model.departement}
              selectedParcelles={selectedParcelles}
              onChangeParcelles={(newSelectedParcelles) =>
                setSelectedParcelles(newSelectedParcelles)
              }
            />
          ),
        [selectedParcelles, model.echelle, model.departement]
      )}
      {useMemo(
        () => (
          <OptionsPanel
            model={model}
            selectedModel={selectedModel}
            layers={layersVisibility}
            onLayerChange={(newLayers) => setLayersVisibility(newLayers)}
            onModeleChange={(newModel) => setModel(newModel)}
          />
        ),
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [model, selectedModel, layersVisibility]
      )}
      <MapControls onAction={handleActions} />
    </>
  )
}

export default Map
