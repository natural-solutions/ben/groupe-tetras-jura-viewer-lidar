import { Grid, Tooltip } from '@nextui-org/react'
import { FC, createElement } from 'react'
import { BiPlus, BiMinus, BiHelpCircle, BiDice1 } from 'react-icons/bi'
import styles from './Controls.module.css'

export type Action = 'zoomIn' | 'zoomOut' | 'fitBounds' | 'help'

export interface MapControlsProps {
  onAction?(action: Action): void
}

const controls = [
  ['Zoomer', 'zoomIn', BiPlus],
  ['Dézoomer', 'zoomOut', BiMinus],
  ['Ajustement aux limites', 'fitBounds', BiDice1],
  ['Aide', 'help', BiHelpCircle],
]

const MapControls: FC<MapControlsProps> = ({ onAction }) => {
  return (
    <div className={styles['map-controls']}>
      <Grid.Container>
        {controls.map((control) => (
          <Grid
            key={control[1] as string}
            onClick={() => onAction && onAction(control[1] as Action)}
          >
            <Tooltip content={control[0]} placement="topEnd">
              <div className={styles['map-controls-button']}>
                {createElement(control[2], { size: 18 })}
              </div>
            </Tooltip>
          </Grid>
        ))}
      </Grid.Container>
    </div>
  )
}

export default MapControls
