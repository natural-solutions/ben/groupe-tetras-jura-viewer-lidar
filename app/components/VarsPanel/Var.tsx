import { FC, forwardRef, useImperativeHandle, useRef } from 'react'
import { Grid, Text, Tooltip } from '@nextui-org/react'
import {
  BiCheckboxChecked,
  BiCheckbox,
  BiGridVertical,
  BiHelpCircle,
} from 'react-icons/bi'
import styles from './Var.module.css'
import {
  DragSource,
  DropTarget,
  DropTargetConnector,
  DragSourceConnector,
  ConnectDropTarget,
  ConnectDragSource,
  DropTargetMonitor,
  DragSourceMonitor,
} from 'react-dnd'
import { XYCoord } from 'dnd-core'
import variables_used from '../../constants/variables_used'

interface VarProps {
  variable: string
  valueMassif: number
  valueParcelles: number
  title: string
  description: string
  index: number
  moveVar: (dragIndex: number, hoverIndex: number) => void
  isDragging: boolean
  connectDragSource: ConnectDragSource
  connectDropTarget: ConnectDropTarget
}

interface VarHeaderProps {
  title: string
  description: string
}

const VarHeader: FC<VarHeaderProps> = ({ title, description }) => {
  return (
    <Grid.Container alignItems="flex-start" gap={0.4}>
      <Grid className={styles['vars-panel-var-header-cursor']}>
        <BiGridVertical />
      </Grid>
      <Grid className={styles['vars-panel-var-header-help']}>
        <Tooltip
          hideArrow
          color="primary"
          content={
            <div style={{ maxWidth: 300, padding: 6 }}>{description}</div>
          }
          placement="rightStart"
        >
          <BiHelpCircle />
        </Tooltip>
      </Grid>
      <Grid xs>
        <Text h3 className={styles['vars-panel-var-header-title']}>
          {title}
        </Text>
      </Grid>
    </Grid.Container>
  )
}

interface VarContentProps {
  variable: string
  valueMassif: number
  valueParcelles: number
}

const VarContent: FC<VarContentProps> = ({
  variable,
  valueMassif,
  valueParcelles,
}) => {
  return (
    <div className={styles['vars-panel-var-content']}>
      <Grid.Container alignItems="center">
        <Grid xs>
          <Text span>{valueParcelles?.toFixed(2)}</Text>
        </Grid>
        <Grid xs>
          <Text span>{valueMassif?.toFixed(2)}</Text>
        </Grid>
        <Grid xs>
          <Text span>
            {variables_used.variable_used_gel?.includes(variable) ? (
              <Tooltip
                color="primary"
                content="Variable essentielle pour la Gélinotte des bois"
              >
                <BiCheckboxChecked color="#228B22" size={25} />
              </Tooltip>
            ) : (
              <Tooltip
                color="primary"
                content="Variable essentielle pour la Gélinotte des bois"
              >
                <BiCheckbox size={25} />
              </Tooltip>
            )}
          </Text>
        </Grid>
        <Grid xs>
          <Text span>
            {variables_used.variable_used_gt?.includes(variable) ? (
              <Tooltip
                color="primary"
                content="Variable essentielle pour le Grand tétras"
              >
                <BiCheckboxChecked color="#228B22" size={25} />
              </Tooltip>
            ) : (
              <Tooltip
                color="primary"
                content="Variable essentielle pour le Grand tétras"
              >
                <BiCheckbox size={25} />
              </Tooltip>
            )}
          </Text>
        </Grid>
      </Grid.Container>
    </div>
  )
}

interface VarInstance {
  getNode(): HTMLDivElement | null
}

const Var = forwardRef<HTMLDivElement, VarProps>(function Var(
  {
    variable,
    valueMassif,
    valueParcelles,
    title,
    description,
    connectDragSource,
    connectDropTarget,
  },
  ref
) {
  const elementRef = useRef(null)
  connectDragSource(elementRef)
  connectDropTarget(elementRef)

  useImperativeHandle<any, VarInstance>(ref, () => ({
    getNode: () => elementRef.current,
  }))

  return (
    <div ref={elementRef} className={styles['vars-panel-var']}>
      <VarHeader title={title} description={description} />
      <VarContent
        variable={variable}
        valueMassif={valueMassif}
        valueParcelles={valueParcelles}
      />
    </div>
  )
})

export const ItemTypes = {
  VAR: 'var',
}

export interface CardDragObject {
  id: string
  index: number
}

export default DropTarget(
  ItemTypes.VAR,
  {
    hover(props: VarProps, monitor: DropTargetMonitor, component: VarInstance) {
      if (!component) {
        return null
      }
      const node = component.getNode()

      if (!node) {
        return null
      }

      const dragIndex = monitor.getItem<CardDragObject>().index
      const hoverIndex = props.index

      if (dragIndex === hoverIndex) {
        return
      }

      const hoverBoundingRect = node.getBoundingClientRect()

      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

      const clientOffset = monitor.getClientOffset()

      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }

      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }

      props.moveVar(dragIndex, hoverIndex)
      monitor.getItem<CardDragObject>().index = hoverIndex
    },
  },
  (connect: DropTargetConnector) => ({
    connectDropTarget: connect.dropTarget(),
  })
)(
  DragSource(
    ItemTypes.VAR,
    {
      beginDrag: (props: VarProps) => ({
        id: props.index,
        index: props.index,
      }),
    },
    (connect: DragSourceConnector, monitor: DragSourceMonitor) => ({
      connectDragSource: connect.dragSource(),
      isDragging: monitor.isDragging(),
    })
  )(Var)
)
