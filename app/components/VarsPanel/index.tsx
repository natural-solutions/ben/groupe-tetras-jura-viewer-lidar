// @ts-nocheck
import { FC, useEffect } from 'react'
import styles from './index.module.css'
import { Avatar, Grid, Spacer, Text, Tooltip } from '@nextui-org/react'
import { BiXCircle } from 'react-icons/bi'
import RSC from 'react-scrollbars-custom'
import Var from './Var'
import update from 'immutability-helper'
import variables, {
  VariableColumns,
  Variables,
} from '../../constants/variables'
import { useLocalStorage } from 'react-use'
import axios from 'axios'
import { Departement } from '../OptionsPanel'

function median(values: number[]) {
  if (values.length === 0) return 0

  values.sort(function (a, b) {
    return a - b
  })

  var half = Math.floor(values.length / 2)

  if (values.length % 2) return values[half]

  return (values[half - 1] + values[half]) / 2.0
}

export interface ParcelleSelected {
  value: string
  label: string
}

export interface VarsPanelProps {
  echelle: string
  departement: Departement
  selectedParcelles: ParcelleSelected[]
  onChangeParcelles?(newSelectedParcelles: ParcelleSelected[]): void
}

const getParcellesData = async (
  echelle: string,
  departement: string,
  parcelles: ParcelleSelected[],
  vars: Variables
) => {
  const result = {}

  for (let parcelle of parcelles) {
    const { data } = await axios.get(
      `/data/variables_cadastre/${parcelle.value}.json`
    )

    for (let variable of vars) {
      const keys = Object.keys(data)
        .filter((key) => key.includes('median'))
        .filter((key) => key.includes(`${echelle}ha`))
        .filter((key) => key.includes(variable[0]))
        .filter((key) => key.includes(departement))
        .filter((key) => data[key] !== null)

      if (keys.length === 1) {
        if (!result[variable[0]]) {
          result[variable[0]] = []
        }

        result[variable[0]].push(data[keys[0]])
      }
    }
  }

  for (let key in result) {
    result[key] = median(result[key])
  }

  return result
}

const VarsPanel: FC<VarsPanelProps> = ({
  echelle,
  departement,
  selectedParcelles,
  onChangeParcelles,
}) => {
  const [data, setData] = useLocalStorage('dataVariables', {})
  const [vars, setVars] = useLocalStorage('variables', variables)
  const handleRemoveParcelle = (parcelle: { value: string }) => {
    if (onChangeParcelles) {
      onChangeParcelles(
        selectedParcelles.filter((p: { value: string }) => {
          return p.value !== parcelle.value
        })
      )
    }
  }

  useEffect(() => {
    if (vars) {
      getParcellesData(echelle, departement, selectedParcelles, vars).then(
        (newData) => {
          setData(newData)
        }
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedParcelles, vars, echelle, departement])

  const moveVar = (dragIndex: number, hoverIndex: number) => {
    if (!vars) return

    const dragCard = vars[dragIndex]
    setVars(
      update(vars, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, dragCard],
        ],
      })
    )
  }

  return (
    <div data-tour="6" className={styles['vars-panel']}>
      <RSC className={styles['vars-panel-wrapper']}>
        <div className={styles['vars-panel-header']}>
          <Text b>Paramètres environnementaux</Text>
          <hr />
          <Text b>Parcelles sélectionnées :</Text>
          <Spacer />
          <Grid.Container direction="column" gap={0.2}>
            {selectedParcelles.map(
              (parcelle: { value: string; label: string }) => (
                <Grid key={parcelle.value} xs>
                  <Grid.Container alignItems="center" gap={0.5}>
                    <Grid>
                      <Tooltip
                        content="Supprimer"
                        color="error"
                        placement="rightStart"
                        onClick={() => {
                          handleRemoveParcelle(parcelle)
                        }}
                      >
                        <Avatar
                          bordered
                          size="mini"
                          color="error"
                          icon={<BiXCircle color="#fff" />}
                        />
                      </Tooltip>
                    </Grid>
                    <Grid>
                      <Text span b size={'.8rem'}>
                        {parcelle.label}
                      </Text>
                    </Grid>
                  </Grid.Container>
                </Grid>
              )
            )}
          </Grid.Container>
        </div>
        <div className={styles['vars-panel-content']}>
          <Grid.Container>
            <Grid>
              {vars &&
                vars.map((variable, index) => (
                  <Var
                    key={variable[VariableColumns.Name]}
                    index={index}
                    moveVar={moveVar}
                    variable={variable[VariableColumns.Name]}
                    valueMassif={data[variable[VariableColumns.Name]]}
                    valueParcelles={data[variable[VariableColumns.Name]]}
                    title={variable[VariableColumns.Title]}
                    description={variable[VariableColumns.Description]}
                  />
                ))}
            </Grid>
          </Grid.Container>
        </div>
      </RSC>
    </div>
  )
}

export default VarsPanel
