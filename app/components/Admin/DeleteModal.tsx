import { FC } from 'react'
import { Text, Modal, Button } from '@nextui-org/react'

export interface AdminUserFormProps {
  onDelete?(): void
  onCancel?(): void
}

const AdminDeleteModal: FC<AdminUserFormProps> = ({
  onDelete = () => {},
  onCancel = () => {},
}) => {
  return (
    <>
      <Modal.Body>
        <Text>
          Êtes-vous sûr de vouloir supprimer les utilisateurs sélectionnés ?
        </Text>
      </Modal.Body>
      <Modal.Footer>
        <Button auto flat onClick={() => onCancel()}>
          Annuler
        </Button>
        <Button auto color="error" onClick={() => onDelete()}>
          Supprimer
        </Button>
      </Modal.Footer>
    </>
  )
}

export default AdminDeleteModal
