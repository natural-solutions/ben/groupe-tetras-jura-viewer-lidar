import { FC } from 'react'
import { Button, Grid } from '@nextui-org/react'

const fn = () => {}

export interface AdminToolbarProps {
  onCreate?(): void
  onDelete?(): void
}

const AdminToolbar: FC<AdminToolbarProps> = ({
  onCreate = fn,
  onDelete = fn,
}) => {
  return (
    <Grid.Container gap={1} justify="flex-end">
      <Grid>
        <Button size="small" onClick={() => onCreate()}>
          Ajouter
        </Button>
      </Grid>
      <Grid>
        <Button size="small" color="error" onClick={() => onDelete()}>
          Supprimer
        </Button>
      </Grid>
      <Grid xs />
    </Grid.Container>
  )
}

export default AdminToolbar
