import { Checkbox, Grid, Spacer, Text, Modal, Button } from '@nextui-org/react'
import Head from 'next/head'
import AdminToolbar from '../Admin/Toolbar'
import LayoutAdmin from '../Layout/Admin'
import { FC, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BiCheckCircle } from 'react-icons/bi'
import { RootState, Dispatch } from '../../store'
import AdminUserForm from './User'
import { User } from '../../models/users'
import AdminDeleteModal from './DeleteModal'

const newUser: User = {
  id: undefined,
  is_active: true,
  is_superuser: false,
  password: '',
  email: '',
}

const AdminPage: FC = () => {
  const state = useSelector((state: RootState) => state.users)
  const dispatch = useDispatch<Dispatch>()
  const [selected, setSelected] = useState<string[]>([])
  const [open, setOpen] = useState<boolean>(false)
  const [deleteModal, setDeleteModal] = useState<boolean>(false)
  const isAllSelected =
    state.users.length > 0 && selected.length === state.users.length

  const handleChange = (value: string) => {
    if (value === 'all') {
      setSelected(
        selected.length === state.users.length
          ? []
          : state.users.map((u) => u.email)
      )
      return
    }

    if (selected.indexOf(value) !== -1) {
      const newSelected = selected.filter((s) => s !== value)
      setSelected(newSelected)
    } else {
      setSelected([...selected, value])
    }
  }

  const handleClose = () => {
    setOpen(false)
    setDeleteModal(false)
  }

  const handleNew = () => {
    setOpen(true)
    dispatch.users.activeUser(newUser)
  }

  const handleDelete = () => {
    handleClose()
    dispatch.users.removeUsers(selected)
  }

  const handleShowDeleteModal = () => {
    setOpen(true)
    setDeleteModal(true)
  }

  const handleEdit = (user: User) => {
    setOpen(true)
    dispatch.users.activeUser(user)
  }

  const handleUserChange = (id: string | undefined, data: User | undefined) => {
    handleClose()
    dispatch.users.createUser({ id, data })
  }

  useEffect(() => {
    dispatch.users.fetchUsers()
  }, [dispatch])

  return (
    <>
      <Head>
        <title>Lidar Viewer | Admin · Groupe Tétras Jura</title>
      </Head>
      <LayoutAdmin>
        <AdminToolbar
          onCreate={() => handleNew()}
          onDelete={() => handleShowDeleteModal()}
        />
        <Spacer y={1} />
        <hr />
        <Spacer y={1} />
        <Grid.Container gap={2} alignItems="center">
          <Grid>
            <Checkbox
              checked={isAllSelected}
              onChange={() => handleChange('all')}
            />
          </Grid>
          <Grid xs>Courrier électronique</Grid>
          <Grid>Administrateur</Grid>
          <Grid></Grid>
        </Grid.Container>
        {state.users.map((user) => (
          <Grid.Container key={user.email} gap={2} alignItems="center">
            <Grid>
              <Checkbox
                checked={selected.indexOf(user.email) > -1}
                onChange={() => handleChange(user.email)}
              />
            </Grid>
            <Grid xs>
              <Button
                style={{ textAlign: 'left', padding: 0 }}
                light
                onClick={() => handleEdit(user)}
              >
                {user.email}
              </Button>
            </Grid>
            <Grid>
              <Text>
                {user.is_superuser ? (
                  <BiCheckCircle color="green" size="22" />
                ) : (
                  ''
                )}
              </Text>
            </Grid>
          </Grid.Container>
        ))}
      </LayoutAdmin>
      <Modal blur closeButton open={open} onClose={() => handleClose()}>
        {deleteModal ? (
          <AdminDeleteModal
            onCancel={() => handleClose()}
            onDelete={() => handleDelete()}
          />
        ) : (
          <AdminUserForm
            user={state.user || newUser}
            onChange={(user) => handleUserChange(user?.id, user)}
          />
        )}
      </Modal>
    </>
  )
}
export default AdminPage
