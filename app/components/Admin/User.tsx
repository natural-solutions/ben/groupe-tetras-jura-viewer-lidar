import { FC, useState } from 'react'
import { Checkbox, Input, Modal, Button } from '@nextui-org/react'
import { User } from '../../models/users'

export interface AdminUserFormProps {
  user: User
  onChange?(user?: User): void
}

const AdminUserForm: FC<AdminUserFormProps> = ({
  user,
  onChange = () => {},
}) => {
  const [values, setValues] = useState<User>(user)
  const handleChange = (field: string, value: any) => {
    setValues({ ...values, [field]: value })
  }

  return (
    <>
      <Modal.Body>
        <Input
          clearable
          bordered
          fullWidth
          placeholder="Adresse email"
          size="large"
          value={values.email}
          onChange={(e) => handleChange('email', e.target.value)}
        />
        <Input
          clearable
          bordered
          fullWidth
          size="large"
          type="password"
          placeholder="Mot de passe"
          initialValue={values.password}
          onChange={(e) => handleChange('password', e.target.value)}
        />
        <Checkbox
          initialChecked={values.is_superuser}
          onChange={(e) => handleChange('is_superuser', e.target.checked)}
        >
          Administrateur
        </Checkbox>
      </Modal.Body>
      <Modal.Footer>
        <Button auto onClick={() => onChange(values)}>
          Create
        </Button>
      </Modal.Footer>
    </>
  )
}

export default AdminUserForm
