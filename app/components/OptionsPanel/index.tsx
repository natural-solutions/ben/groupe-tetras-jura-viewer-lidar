import { FC, useEffect, useState } from 'react'
import { Button, Grid, Spacer, Switch, Text, Tooltip } from '@nextui-org/react'
import styles from './index.module.css'
import Legend from './Legend'
import ZonesProtegees from './ZonesProtegees'
import { BiChevronLeft, BiChevronRight } from 'react-icons/bi'
import RSC from 'react-scrollbars-custom'
import { useLocalStorage } from 'react-use'

export type Layers = Record<string, boolean>

interface OptionsPanelProps {
  selectedModel: string
  model: Modele
  layers: Layers | undefined
  onLayerChange(newLayers: Layers): void
  onModeleChange(newModel: Modele): void
}

export type Departement = 'AIN' | 'JURA'
type Espece = 'GT' | 'GEL'
type Echelle = '1_8' | '15_8'
type Saison = 'summer' | 'winter'
type Sexe = 'COQ' | 'POULE'

interface Modele {
  display: boolean
  departement: Departement
  espece: Espece
  saison: Saison
  sexe: Sexe
  echelle: Echelle
}

export type Level = 'echec' | 'pauvre' | 'moyen' | 'bon' | 'excellent'
export type LevelItem = {
  label: string
  color: string
}

export type LevelItems = Record<Level, LevelItem>

export const levelItems: LevelItems = {
  echec: {
    color: '#f52b2d',
    label: 'Échec',
  },
  pauvre: {
    color: '#ef9456',
    label: 'Pauvre',
  },
  moyen: {
    color: '#fccc39',
    label: 'Moyen',
  },
  bon: {
    color: '#9eca7f',
    label: 'Bon',
  },
  excellent: {
    color: '#83b75e',
    label: 'Excellent',
  },
}

export const AUC: Record<string, number> = {
  modeles_1_8ha_GEL_AIN: 0.7,
  modeles_1_8ha_GEL_JURA: 0.62,
  modeles_15_8ha_GEL_AIN: 0.61,
  modeles_15_8ha_GEL_JURA: 0.75,
  modeles_1_8ha_GT_COQ_winter_AIN: 0.78,
  modeles_1_8ha_GT_COQ_winter_JURA: 0.74,
  modeles_1_8ha_GT_POULE_winter_AIN: 0.73,
  modeles_1_8ha_GT_POULE_winter_JURA: 0.72,
  modeles_1_8ha_GT_summer_AIN: 0.81,
  modeles_1_8ha_GT_summer_JURA: 0.74,
  modeles_15_8ha_GT_COQ_winter_AIN: 0.78,
  modeles_15_8ha_GT_COQ_winter_JURA: 0.75,
  modeles_15_8ha_GT_POULE_winter_AIN: 0.72,
  modeles_15_8ha_GT_POULE_winter_JURA: 0.73,
  modeles_15_8ha_GT_summer_AIN: 0.83,
  modeles_15_8ha_GT_summer_JURA: 0.75,
}

export const initModel: Modele = {
  display: true,
  departement: 'AIN',
  echelle: '1_8',
  espece: 'GT',
  saison: 'winter',
  sexe: 'COQ',
}

export const OPTIONAL_LAYERS = [
  ['massifs', 'Zones de présence de Grand tétras (PNA 2020)'],
  ['satellite', 'Vue satellite'],
  ['parcelles', 'Parcelles cadastrales'],
]

const OptionsPanel: FC<OptionsPanelProps> = ({
  selectedModel,
  model,
  layers,
  onLayerChange,
  onModeleChange,
}) => {
  const [auc, setAUC] = useState<number>(AUC[selectedModel])
  const [level, setLevel] = useState<Level>('echec')
  const [visible, setVisible] = useState<boolean>(true)
  const [models, setModels] = useLocalStorage<boolean>('models', true)

  useEffect(() => {
    const newAUC = AUC[selectedModel]
    setAUC(newAUC)

    switch (true) {
      case newAUC < 0.6:
        setLevel('echec')
        break
      case newAUC > 0.6 && newAUC < 0.7:
        setLevel('pauvre')
        break
      case newAUC > 0.7 && newAUC < 0.8:
        setLevel('moyen')
        break
      case newAUC > 0.8 && newAUC < 0.9:
        setLevel('bon')
        break
      case newAUC > 0.9:
        setLevel('excellent')
        break
    }
  }, [selectedModel])

  useEffect(() => {
    if (models !== undefined) {
      onModeleChange({ ...model, display: models })
    } else {
      onModeleChange({ ...model, display: false })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [models])

  const handleChangeLayer = (layerName: string) => {
    if (layers && Object.keys(layers).includes(layerName)) {
      onLayerChange({
        ...layers,
        [layerName]: !layers[layerName],
      })
    }
  }

  const handleSwitchModel = () => {
    setModels(!models)
  }

  return (
    <div className={styles['options-panel']}>
      <div
        className={`${styles['options-panel-wrapper']} ${
          visible ? styles['options-panel-wrapper-active'] : ''
        }`}
      >
        <Tooltip content={visible ? 'Fermer' : 'Ouvrir'} placement="bottomEnd">
          <button
            className={styles['options-panel-toggle']}
            onClick={() => {
              setVisible(!visible)
            }}
          >
            {visible ? (
              <BiChevronRight size={36} />
            ) : (
              <BiChevronLeft size={36} />
            )}
          </button>
        </Tooltip>
        <div
          className={`${styles['options-panel-content']} ${
            visible ? styles['options-panel-content-active'] : ''
          }`}
        >
          <RSC permanentTrackY>
            <div className={styles['options-panel-content-wrapper']}>
              <Spacer y={0.5} />
              <Grid.Container alignItems="center" gap={1}>
                <Grid>
                  <Switch
                    checked={models}
                    color="error"
                    onChange={() => handleSwitchModel()}
                  />
                </Grid>
                <Grid
                  style={{ cursor: 'pointer' }}
                  onClick={() => handleSwitchModel()}
                >
                  {`Cartes LiDAR "Tétraoninés"`}
                </Grid>
              </Grid.Container>
              <Spacer />

              <Grid.Container direction="column">
                {models && (
                  <Grid>
                    <Grid.Container
                      data-tour="3"
                      direction="column"
                      alignItems="flex-end"
                      className="options-panel-content-modeles"
                      gap={0.2}
                    >
                      <Grid xs>
                        <Button
                          color={model.echelle === '15_8' ? 'error' : '#000'}
                          light={model.echelle === '1_8'}
                          size="small"
                          onClick={() =>
                            onModeleChange({ ...model, echelle: '15_8' })
                          }
                        >
                          Grande échelle
                        </Button>
                        <Button
                          color={model.echelle === '1_8' ? 'error' : '#000'}
                          light={model.echelle === '15_8'}
                          size="small"
                          onClick={() =>
                            onModeleChange({ ...model, echelle: '1_8' })
                          }
                        >
                          Petite échelle
                        </Button>
                      </Grid>
                      <Grid xs>
                        <Button
                          size="small"
                          color={model.espece === 'GEL' ? 'error' : '#000'}
                          light={model.espece === 'GT'}
                          onClick={() =>
                            onModeleChange({ ...model, espece: 'GEL' })
                          }
                        >
                          Gélinotte des bois
                        </Button>
                        <Button
                          size="small"
                          color={model.espece === 'GT' ? 'error' : '#000'}
                          light={model.espece === 'GEL'}
                          onClick={() =>
                            onModeleChange({ ...model, espece: 'GT' })
                          }
                        >
                          Grand tétras
                        </Button>
                      </Grid>
                      <Grid xs>
                        <Button
                          color={
                            model.departement === 'JURA' ? 'error' : '#000'
                          }
                          light={model.departement === 'AIN'}
                          size="small"
                          onClick={() =>
                            onModeleChange({ ...model, departement: 'JURA' })
                          }
                        >
                          Jura
                        </Button>
                        <Button
                          color={model.departement === 'AIN' ? 'error' : '#000'}
                          light={model.departement === 'JURA'}
                          size="small"
                          onClick={() =>
                            onModeleChange({ ...model, departement: 'AIN' })
                          }
                        >
                          Ain
                        </Button>
                      </Grid>
                      <Grid xs>
                        <Button
                          size="small"
                          disabled={model.espece === 'GEL'}
                          color={model.saison === 'summer' ? 'error' : '#000'}
                          light={model.saison === 'winter'}
                          onClick={() =>
                            onModeleChange({ ...model, saison: 'summer' })
                          }
                        >
                          Été
                        </Button>
                        <Button
                          disabled={model.espece === 'GEL'}
                          size="small"
                          color={model.saison === 'winter' ? 'error' : '#000'}
                          light={model.saison === 'summer'}
                          onClick={() =>
                            onModeleChange({ ...model, saison: 'winter' })
                          }
                        >
                          Hiver
                        </Button>
                      </Grid>
                      <Grid data-tour="4" xs>
                        <Button
                          disabled={
                            model.espece !== 'GT' || model.saison !== 'winter'
                          }
                          color={model.sexe === 'COQ' ? 'error' : '#000'}
                          light={model.sexe === 'POULE'}
                          onClick={() =>
                            onModeleChange({ ...model, sexe: 'COQ' })
                          }
                          size="small"
                        >
                          Coq
                        </Button>
                        <Button
                          size="small"
                          disabled={
                            model.espece !== 'GT' || model.saison !== 'winter'
                          }
                          color={model.sexe === 'POULE' ? 'error' : '#000'}
                          light={model.sexe === 'COQ'}
                          onClick={() =>
                            onModeleChange({ ...model, sexe: 'POULE' })
                          }
                        >
                          Poule
                        </Button>
                      </Grid>
                    </Grid.Container>

                    <hr />

                    <Grid className="options-panel-content-auc">
                      <Text h3 span>
                        Fiabilité du modèle :
                      </Text>
                      <Grid.Container>
                        <Text span b color={levelItems[level].color}>
                          {levelItems[level].label}
                        </Text>
                        <Text span>&nbsp;(AUC={auc})</Text>
                      </Grid.Container>
                    </Grid>

                    <hr />

                    <Grid className="options-panel-content-legend">
                      <Legend />
                    </Grid>

                    <hr />
                  </Grid>
                )}

                {layers && (
                  <Grid>
                    <ZonesProtegees
                      active={layers['zonesProtegees']}
                      onChange={() => handleChangeLayer('zonesProtegees')}
                    />
                  </Grid>
                )}

                {layers &&
                  OPTIONAL_LAYERS.map((layer) => (
                    <Grid key={layer[0]}>
                      <Grid.Container alignItems="center" gap={1}>
                        <Grid>
                          <Switch
                            checked={layers[layer[0]]}
                            color="error"
                            onChange={() => {
                              onLayerChange({
                                ...layers,
                                [layer[0]]: !layers[layer[0]],
                              })
                            }}
                          />
                        </Grid>
                        <Grid xs>
                          <Text
                            span
                            onClick={() =>
                              onLayerChange({
                                ...layers,
                                [layer[0]]: !layers[layer[0]],
                              })
                            }
                          >
                            {layer[1]}
                          </Text>
                        </Grid>
                      </Grid.Container>
                    </Grid>
                  ))}
              </Grid.Container>
              <hr />
            </div>
          </RSC>
        </div>
      </div>
    </div>
  )
}

export default OptionsPanel
