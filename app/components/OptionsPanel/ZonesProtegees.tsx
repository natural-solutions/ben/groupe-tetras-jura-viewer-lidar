import { FC } from 'react'
import { Grid, Link, Spacer, Switch } from '@nextui-org/react'
import ZONES_PROTEGEES, {
  ZonesProtegeesColumn,
} from '../../constants/zonesProtegees'

interface ZonesProtegeesProps {
  active: boolean
  onChange?(val: boolean): void
}

const lengendStyle = {
  width: 25,
  height: 25,
  marginRight: 16,
  borderRadius: '100%',
  border: '1px solid #cecece',
}

const ZonesProtegees: FC<ZonesProtegeesProps> = ({ active, onChange }) => {
  return (
    <>
      <Grid.Container alignItems="center" gap={1}>
        <Grid>
          <Switch
            checked={active}
            onChange={() => onChange && onChange(!active)}
          />
        </Grid>
        <Grid
          onClick={() => onChange && onChange(!active)}
          style={{ cursor: 'pointer' }}
        >
          {`Zones naturelles protégées ${active ? ':' : ''}`}
        </Grid>
        <Grid>
          <Link
            icon
            target="_blank"
            href="http://www.bourgogne-franche-comte.developpement-durable.gouv.fr/IMG/pdf/plaquette_appb_forets_altitude_haut_jura_cle73afd4.pdf"
          />
        </Grid>
      </Grid.Container>
      {active && (
        <>
          <Spacer />
          <Grid.Container direction="column" gap={1}>
            {ZONES_PROTEGEES.map((zone) => (
              <Grid key={zone[ZonesProtegeesColumn.name]}>
                <Grid.Container direction="row" alignItems="center">
                  <Grid
                    style={{
                      ...lengendStyle,
                      background: zone[ZonesProtegeesColumn.color],
                    }}
                  >
                    &nbsp;
                  </Grid>
                  <Grid
                    xs
                    style={{
                      fontSize: '.9rem',
                      padding: '.8',
                    }}
                  >
                    {zone[ZonesProtegeesColumn.name]}
                  </Grid>
                  <Grid>
                    <Link
                      icon
                      target="_blank"
                      href={zone[ZonesProtegeesColumn.link]}
                    />
                  </Grid>
                </Grid.Container>
              </Grid>
            ))}
          </Grid.Container>
          <Spacer />
        </>
      )}
    </>
  )
}

export default ZonesProtegees
