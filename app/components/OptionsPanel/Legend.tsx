import { FC } from 'react'
import styles from './Legend.module.css'
import { Text } from '@nextui-org/react'

export interface LegendColorProps {
  color?: string
}

export const LegendColor: FC<LegendColorProps> = ({ color = '#000' }) => {
  return (
    <div
      className={styles['legend-color']}
      style={{ backgroundColor: color }}
    />
  )
}

export interface LegendProps {}

const Legend: FC<LegendProps> = () => {
  return (
    <>
      <Text h3>Légende</Text>
      <div className={styles['legend']}>
        <div>Très bon</div>
        <LegendColor color="rgb(0, 147, 156)" />
        <LegendColor color="rgb(93, 186, 191)" />
        <LegendColor color="rgb(186, 225, 226)" />
        <LegendColor color="rgb(248, 192, 170)" />
        <LegendColor color="rgb(221, 119, 85)" />
        <LegendColor color="rgb(194, 46, 0)" />
        <div>Mauvais</div>
      </div>
    </>
  )
}

export default Legend
