import { FC } from 'react'
import { Spacer } from '@nextui-org/react'
import LayoutMain from '../Layout/Main'

interface LayoutDefaultProps {}

const LayoutDefault: FC<LayoutDefaultProps> = ({ children }) => {
  return (
    <LayoutMain fluid={false}>
      <Spacer />
      {children}
    </LayoutMain>
  )
}

export default LayoutDefault
