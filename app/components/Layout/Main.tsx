import { Container } from '@nextui-org/react'
import { FC, useEffect } from 'react'
import LayoutHeader from './Header'
import styles from './Main.module.css'
import { useRouter } from 'next/router'
import { useLocalStorage } from 'react-use'

interface LayoutDefaultProps {
  header?: boolean
  fluid?: boolean
  background?: string
}

const LayoutDefault: FC<LayoutDefaultProps> = ({
  header = true,
  fluid = true,
  background = '#fff',
  children,
}) => {
  const [token] = useLocalStorage('token', '', { raw: true })
  const router = useRouter()

  useEffect(() => {
    if (!token && router.pathname !== '/login') {
      router.push('/login')
    }
  }, [token, router])

  return (
    <Container
      className={styles['main-container']}
      as="main"
      fluid
      display="flex"
      direction="column"
      wrap="wrap"
      style={{ background: background }}
    >
      {header && <LayoutHeader />}
      <Container className={styles['children-container']} fluid={fluid}>
        {children}
      </Container>
    </Container>
  )
}

export default LayoutDefault
