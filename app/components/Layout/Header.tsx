import { FC, useEffect } from 'react'
import { Grid, Link, Text, Tooltip, Button } from '@nextui-org/react'
import styles from './Header.module.css'
import { useRouter } from 'next/dist/client/router'
import { BiMap, BiExit } from 'react-icons/bi'
import { useDispatch, useSelector } from 'react-redux'
import { RootState, Dispatch } from '../../store'
import { useLocalStorage } from 'react-use'

interface LayoutHeaderProps {
  title?: string
}

const LayoutHeader: FC<LayoutHeaderProps> = ({
  title = 'Groupe Tétras Jura',
}) => {
  const [, setToken] = useLocalStorage('token')
  const state = useSelector((state: RootState) => state.users)
  const dispatch = useDispatch<Dispatch>()
  const router = useRouter()
  const isHome = router.route === '/'

  const link = {
    href: isHome ? '/admin' : '/',
    title: isHome ? 'Admin' : 'Carto',
  }

  const handleLogout = () => {
    setToken('')
    window.location.href = '/login'
  }

  useEffect(() => {
    if (!state.me && !state.isLoading) {
      dispatch.users.fetchMe()
    }
  }, [dispatch, state.me, state.isLoading])

  return (
    <header className={styles.header}>
      <Grid.Container alignItems="center">
        <Grid xs data-tour="1">
          <Text h1 size={'1.6rem'}>
            {title}
          </Text>
        </Grid>
        <Grid>
          <Grid.Container gap={1} justify="center" alignItems="center">
            {state.me?.is_superuser && (
              <Grid>
                <Link block color="#fff" href={link.href}>
                  <Grid.Container alignItems="center" gap={0.5}>
                    <Grid>
                      <BiMap />
                    </Grid>
                    <Grid xs>{link.title}</Grid>
                  </Grid.Container>
                </Link>
              </Grid>
            )}
            <Grid>{state.me?.email}</Grid>
            <Grid>
              <Tooltip content="Déconnexion" placement="leftStart">
                <Button
                  auto
                  light
                  icon={<BiExit color="#fff" />}
                  onClick={handleLogout}
                />
              </Tooltip>
            </Grid>
          </Grid.Container>
        </Grid>
      </Grid.Container>
    </header>
  )
}

export default LayoutHeader
