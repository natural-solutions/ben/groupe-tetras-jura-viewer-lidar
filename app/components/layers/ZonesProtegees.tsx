import { GeoJsonLayer } from '@deck.gl/layers'

export default function ZoneProtegees(isVisible: boolean) {
  return new GeoJsonLayer({
    id: 'zonesProtegees',
    data: '/data/zones_protegees.geojson',
    visible: isVisible,
    opacity: 0.8,
    filled: true,
    wireframe: true,
    highlightedObjectIndex: 0,
    getLineWidth: 10,
    getLineColor: [150, 150, 150],
    getFillColor: (f: any) => {
      switch (f.properties.statut) {
        case 'Arrete Prefectoral de Protection de Biotope':
          return [247, 251, 255]
        case 'Reserve Naturelle Nationale de la Haute-Chaine du Jura':
          return [176, 210, 232]
        case 'Zone de Quietude pour la Faune Sauvage':
          return [62, 142, 196]
        default:
          return [255, 0, 0]
      }
    },
    pickable: true,
    autoHighlight: true,
    highlightColor: [255, 200, 0, 100],
  })
}
