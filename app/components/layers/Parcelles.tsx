import { GeoJsonLayer, GeoJsonLayerProps } from '@deck.gl/layers'

const fn = () => {}

export default function Parcelles({
  onClick = fn,
  onDataLoad = fn,
}: GeoJsonLayerProps<any>) {
  return new GeoJsonLayer({
    id: 'parcelles',
    data: '/data/cadastre.geojson',
    opacity: 0.8,
    filled: true,
    wireframe: true,
    highlightedObjectIndex: 0,
    getLineWidth: 10,
    getLineColor: [150, 150, 150, 100],
    getFillColor: [255, 255, 255, 0],
    pickable: true,
    autoHighlight: true,
    highlightColor: [255, 200, 0, 100],
    onClick: onClick,
    onDataLoad: onDataLoad,
  })
}
