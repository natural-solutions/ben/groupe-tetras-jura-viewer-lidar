import { GeoJsonLayer } from '@deck.gl/layers'

export default function Massifs(isVisible: boolean) {
  return new GeoJsonLayer({
    id: 'massifs',
    data: '/data/massifs.geojson',
    visible: isVisible,
    opacity: 0.8,
    filled: true,
    getLineColor: [150, 150, 150],
    getFillColor: [255, 255, 100],
    pickable: true,
  })
}
