import { TileLayer } from '@deck.gl/geo-layers'
import { BitmapLayer } from 'deck.gl'

export default function layer(layerName: string, isVisible: boolean) {
  return new TileLayer({
    id: layerName,
    data: `/tiles/services/${layerName}/tiles/{z}/{x}/{y}.png`,
    visible: isVisible,
    maxZoom: 14,
    opacity: 0.8,
    renderSubLayers: (props) => {
      const {
        bbox: { west, south, east, north },
      } = props.tile

      return new BitmapLayer(props, {
        data: null,
        image: props.data,
        bounds: [west, south, east, north],
      })
    },
  })
}
