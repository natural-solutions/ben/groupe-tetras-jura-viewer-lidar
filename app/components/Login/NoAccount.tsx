import { FC } from 'react'
import { Link, Spacer, Text } from '@nextui-org/react'
import styles from './Form.module.css'

interface LoginNoAccountProps {}

const LoginNoAccount: FC<LoginNoAccountProps> = () => {
  return (
    <div className={styles['login-form']}>
      <Text b>Si vous n&apos;avez pas de compte :</Text>
      <Spacer y={1} />
      <Text>
        La plateforme est soumise à une demande d&apos;accès. Veuillez en
        formuler la demande :
      </Text>
      <Spacer y={1} />
      <Link block color="primary" href="mailto:groupe-tetras@wanadoo.fr">
        Demander l&apos;accès
      </Link>
    </div>
  )
}

export default LoginNoAccount
