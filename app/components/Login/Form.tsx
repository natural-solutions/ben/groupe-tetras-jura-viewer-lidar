import { FC, useContext, useState } from 'react'
import { Button, Grid, Text, Input, Spacer } from '@nextui-org/react'
import styles from './Form.module.css'
import { AuthContext } from '../Auth/Context'
import axios from '../../lib/authAxios'
import { useLocalStorage } from 'react-use'

interface ServerData {
  access_token: string
}

interface LoginFormProps {}

const LoginForm: FC<LoginFormProps> = () => {
  const { setAuthenticated } = useContext(AuthContext)
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState<string | boolean>(false)
  const [, setToken] = useLocalStorage('token', '', { raw: true })

  const handleLogin = async () => {
    try {
      const formData = new FormData()

      formData.set('username', username)
      formData.set('password', password)

      const { status, data } = await axios.post<ServerData>(
        '/auth/jwt/login',
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        }
      )

      if (status === 200) {
        setError(false)
        setAuthenticated(true)
        setToken(data.access_token)
        return
      }
    } catch (e) {
      setError('Adresse électronique ou mot de passe invalide')
    }
  }

  return (
    <div className={styles['login-form']}>
      <Text b>Si vous avez un compte :</Text>
      <Spacer y={2} />
      <Grid.Container direction="column" gap={3}>
        <Grid>
          <Input
            labelPlaceholder="Identifiant"
            clearable
            color="primary"
            width="100%"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Grid>
        <Grid>
          <Input
            labelPlaceholder="Mot de passe"
            color="primary"
            clearable
            type="password"
            width="100%"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Grid>
      </Grid.Container>
      <Spacer />
      {error && (
        <Text p color="error">
          {error}
        </Text>
      )}
      <Spacer />
      <Button onClick={() => handleLogin()}>Connexion</Button>
    </div>
  )
}

export default LoginForm
