import React from 'react'

export interface AuthContext {
  authenticated: boolean
  setAuthenticated: (isAuthenticated: boolean) => void
}

/* istanbul ignore next */
const noop = () => {}

export const AuthContext = React.createContext<AuthContext>({
  authenticated: false,
  setAuthenticated: noop,
})
