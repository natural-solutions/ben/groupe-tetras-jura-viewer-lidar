import { createModel } from '@rematch/core'
import axios from '../lib/authAxios'
import { RootModel } from '.'

export type User = {
  id: string | undefined
  email: string
  is_active: boolean
  is_superuser: boolean
  password?: string
}

interface UserState {
  me?: User
  user?: User
  users: Array<User>
  isLoading: boolean
}

export const users = createModel<RootModel>()({
  state: {
    me: undefined,
    user: undefined,
    users: [],
    isLoading: false,
  } as UserState,
  reducers: {
    requestMe(state) {
      return {
        ...state,
        isLoading: true,
        me: undefined,
      }
    },
    requestUser(state) {
      return {
        ...state,
        isLoading: true,
        user: undefined,
      }
    },
    requestUsers(state) {
      return {
        ...state,
        users: [],
        isLoading: true,
      }
    },
    receiveUser(state, payload) {
      return {
        ...state,
        isLoading: false,
        user: payload,
      }
    },
    receiveUsers(state, payload) {
      return {
        ...state,
        isLoading: false,
        users: payload,
      }
    },
    receiveMe(state, payload) {
      return {
        ...state,
        isLoading: false,
        me: payload,
      }
    },
  },
  effects: (dispatch) => ({
    async createUser(payload, rootState) {
      try {
        this.requestUser()

        const { status, data } = await axios.post(
          '/auth/register',
          payload.data
        )

        if (status === 201) {
          this.receiveUser(data)
          this.receiveUsers([...rootState.users.users, data])

          if (payload.data.is_superuser) {
            dispatch.users.updateUser({ ...payload.data, id: data.id })
          }
          return data
        }
      } catch (err) {
        this.receiveUser(undefined)
      }
    },
    async updateUser(payload, rootState) {
      try {
        const { status, data } = await axios.patch(
          `/users/${payload.id}`,
          payload
        )

        if (status === 200) {
          this.receiveUser(data)
          this.receiveUsers([
            ...rootState.users.users.filter((u) => u.id !== payload.id),
            data,
          ])
        }
      } catch (err) {}
    },
    async fetchUser(payload) {
      try {
        this.requestMe()
        const { status, data } = await axios.get(`/users/${payload}`)

        if (status === 200) {
          this.receiveUser(data)
          return data
        }
      } catch (err) {
        this.receiveUser(undefined)
      }
    },
    async fetchMe() {
      try {
        this.requestMe()
        const { status, data } = await axios.get('/users/me')

        if (status === 200) {
          this.receiveMe(data)
          return data
        }
      } catch (err) {
        this.receiveMe(undefined)
      }
    },
    async fetchUsers() {
      try {
        this.requestUsers()
        const { status, data } = await axios.get('/users')

        if (status === 200) {
          this.receiveUsers(data)
          return data
        }
      } catch (err) {
        this.receiveUsers([])
      }
    },
    activeUser(user: User) {
      this.receiveUser(user)
    },
    async removeUsers(payload, rootState) {
      try {
        const ids = rootState.users.users
          .filter((u) => payload.includes(u.email))
          .map((u) => u.id)

        const removedIds: string[] = []

        for (let id of ids) {
          if (id === rootState.users.me?.id) {
            continue
          }

          const { status } = await axios.delete(`/users/${id}`)

          if (status === 204 && id) {
            removedIds.push(id)
          }
        }

        this.receiveUsers(
          rootState.users.users.filter(
            (u) => u.id && !removedIds.includes(u.id)
          )
        )
      } catch (e) {
        this.receiveUsers(rootState.users.users)
      }
    },
  }),
})
