import '../styles/globals.css'
import 'maplibre-gl/dist/maplibre-gl.css'
import type { AppProps } from 'next/app'
import { AuthProvider } from '../components/Auth/Provider'
import { store } from '../store'
import { Provider } from 'react-redux'

function MyApp({ Component, pageProps }: AppProps) {
  const handleLogin = () => {
    window.location.href = '/'
  }

  const handleLogout = () => {
    window.location.href = '/login'
  }

  return (
    <AuthProvider
      onLogin={handleLogin}
      onLogout={handleLogout}
      defaultAuthenticated={false}
    >
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </AuthProvider>
  )
}
export default MyApp
