import { NextPage } from 'next'
import Head from 'next/head'
import LayoutMain from '../components/Layout/Main'
import dynamic from 'next/dynamic'
import { TourProvider } from '@reactour/tour'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import steps from '../constants/steps'

const Map = dynamic(() => import('../components/Map'), { ssr: false })

const PageHome: NextPage = () => {
  return (
    <DndProvider backend={HTML5Backend}>
      <TourProvider steps={steps}>
        <Head>
          <title>Lidar Viewer · Groupe Tétras Jura</title>
        </Head>
        <LayoutMain>
          <Map />
        </LayoutMain>
      </TourProvider>
    </DndProvider>
  )
}
export default PageHome
