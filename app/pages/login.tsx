import { NextPage } from 'next'
import Head from 'next/head'
import LayoutMain from '../components/Layout/Main'
import { Grid, Text, Spacer, Container } from '@nextui-org/react'
import Image from 'next/image'
import LoginForm from '../components/Login/Form'
import LoginNoAccount from '../components/Login/NoAccount'

const PageLogin: NextPage = () => {
  return (
    <>
      <Head>
        <title>Lidar Viewer | Login · Groupe Tétras Jura</title>
      </Head>
      <LayoutMain background={'#e2deda'} fluid={false} header={false}>
        <Grid.Container direction="column" alignItems="center">
          <Grid>
            <Image
              src="/logo.svg"
              alt="Logo Tétras Jura"
              height="350"
              width="350"
            />
          </Grid>
          <Grid style={{ textAlign: 'center' }}>
            <Text h1 size="1.4rem" color="#a4102e">
              Plateforme de mise à disposition des modèles LiDAR
              &quot;Tétraoninés&quot; et des données spatiales
            </Text>
          </Grid>
          <Grid style={{ textAlign: 'center' }}>
            <Text b color="#707070">
              Outil d&apos;aide à la gestion forestière et à la prise de
              décision sur le massif jurassien
            </Text>
          </Grid>
        </Grid.Container>
        <Spacer y={3} />
        <Grid.Container gap={2}>
          <Grid xs>
            <Container display="flex" justify="center">
              <LoginForm />
            </Container>
          </Grid>
          <Grid xs>
            <Container display="flex" justify="center">
              <LoginNoAccount />
            </Container>
          </Grid>
        </Grid.Container>
        <Spacer y={4} />
        <div style={{ textAlign: 'center' }}>
          <img width="70%" src="/logos_horizontaux.svg" alt="Logos" />
        </div>
      </LayoutMain>
    </>
  )
}

export default PageLogin
