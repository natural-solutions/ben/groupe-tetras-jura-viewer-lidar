import { NextPage } from 'next'
import dynamic from 'next/dynamic'

const AdminPage = dynamic(() => import('../components/Admin/Page'))

const PageAdmin: NextPage = () => {
  return <AdminPage />
}
export default PageAdmin
