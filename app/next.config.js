/** @type {import('next').NextConfig} */
module.exports = {
  images: {
    domains: ['wxs.ign.fr'],
  },
  reactStrictMode: true,
  webpack: (config) => {
    config.resolve.alias['mapbox-gl'] = 'maplibre-gl'
    return config
  },
  webpackDevMiddleware: (config) => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    }
    return config
  },
}
