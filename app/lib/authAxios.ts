import axios from 'axios'

let instance

if (typeof window !== 'undefined' && window.localStorage) {
  const token = localStorage.getItem('token')

  instance = axios.create({
    baseURL: '/api',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })

  instance.interceptors.response.use(
    function (response) {
      return response
    },
    function (error) {
      if (error.response.status === 401) {
        window.location.href = '/login'
      }

      return Promise.reject(error)
    }
  )
}

export default instance || axios
