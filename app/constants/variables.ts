export enum VariableColumns {
  Name = 0,
  Title = 1,
  Description = 2,
}
export type Variable = [string, string, string]
export type Variables = Variable[]

const variables: Variables = [
  [
    'MeanVeg',
    'Hauteur moyenne des arbres (m)',
    'Cette variable renseigne sur la hauteur moyenne de la forêt. On peut ainsi avoir une indication sur la maturité du peuplement forestier. Le Grand tétras cherche des forêts matures qui présenteront une hauteur moyenne importante. Au contraire, la Gélinotte des bois est une espèce qui recherche des forêts plutôt jeunes, avec une hauteur moyenne du peuplement moins élevée.',
  ],
  [
    'CHM.mean',
    'Modèle de la hauteur moyenne des arbres (m)',
    'Cette variable renseigne sur la hauteur moyenne de la forêt. On peut ainsi avoir une indication sur la maturité du peuplement forestier. Le Grand tétras cherche des forêts matures qui présenteront une hauteur moyenne importante. Au contraire, la Gélinotte des bois est une espèce qui recherche des forêts plutôt jeunes, avec une hauteur moyenne du peuplement moins élevée. Cette variable est issue de modélisations, afin de permettre le traitement statistique possible.',
  ],
  [
    'CHM.PercInf0.5',
    'Proportion d’arbres inférieurs à 0,5m (en %)',
    'Cette variable indique si la forêt présente une strate basse bien développée. On cherche ici à décrire la présence de la myrtille, si importante pour le Grand tétras. Attention, il peut y avoir une confusion avec de jeunes semis de hêtre, qui sont au contraire néfastes au Grand tétras et à la Gélinotte.',
  ],
  [
    'CHM.PercInf1',
    'Proportion d’arbres inférieurs à 1m (en %)',
    'Combinée à l’information précédente, cela permet de préciser s’il s’agit de hêtre ou de myrtille. (Cette dernière ne dépassant que très rarement 50 cm de haut dans le Jura.)',
  ],
  [
    'CHM.Perc1_5',
    'Proportion d’arbres de 1 à 5m (en %)',
    'Cette variable renseigne sur le niveau de fermeture de la forêt entre 1 et 5 mètres. Le Grand tétras cherche des forêts ouvertes alors que la Gélinotte des bois apprécie les forêts denses.',
  ],
  [
    'CHM.Perc2_5',
    'Proportion d’arbres de 2 à 5m (en %)',
    'Cette variable renseigne sur le niveau de fermeture de la forêt entre 2 et 5 mètres. Le Grand tétras cherche des forêts ouvertes alors que la Gélinotte des bois apprécie les forêts denses.',
  ],
  [
    'CHM.PercSup5',
    'Proportion des arbres supérieurs à 5m (en %)',
    'Cette information montre la présence de perchis. Nécessaire au renouvellement de la forêt, il ne doit pas être trop présent pour le Grand tétras car il ferme le sous-étage et limite l’arrivée de la lumière au sol. Au contraire, la Gélinotte qui apprécie les sous étages plus denses.',
  ],
  [
    'CHM.PercSup10',
    'Proportion d’arbres supérieurs à 10m (en %)',
    'Il s’agit ici de décrire le niveau de fermeture de la jeune futaie mais aussi le niveau de fermeture de la forêt. Une forêt très dense dans ces strates aura des difficultés à laisser pénétrer la lumière, indispensable au bond développement de la ressource alimentaire des tétraoninés.',
  ],
  [
    'CHM.PercSup20',
    'Proportion d’arbres supérieurs à 20m (en %)',
    'Une majorité d’arbres de plus de 20 mètres dans une forêt montre la maturité du peuplement.',
  ],
  [
    'Tree_density',
    'Densité des arbres (nombre/ha)',
    'Il s’agit du nombre total de tiges par hectare. Un nombre important est synonyme de forêt dense qui laisse peu de place à la lumière. La strate basse aura donc du mal à se développer, ce qui est défavorable aux Tétraoninés.',
  ],
  [
    'Tree_density10',
    'Densité d’arbres entre 10 et 20m (nombre/ha)',
    'Il s’agit du nombre de tiges entre 10 et 20 mètres de hauteur par hectare. Un nombre important est synonyme de forêt dense qui laisse peu de place à la lumière. La strate basse aura donc du mal à se développer, ce qui est défavorable aux Tétraoninés.',
  ],
  [
    'Tree_density20',
    'Densité d’arbres entre 20 et 30 mètres (nombre/ha)',
    'Il s’agit du nombre de tiges supérieures à 20 mètres de hauteur par hectare. Un nombre important est synonyme de forêt dense, avec un volume sur pied important. La lumière pénètre peu et la strate basse se développe difficilement, à moins que des clairières soient présentes.',
  ],
  [
    'Tree_density30',
    'Densité d’arbres supérieurs à 30 mètres (nombre/ha)',
    'Il s’agit du nombre de tiges supérieures à 30 mètres de hauteur par hectare. Un nombre important est synonyme de forêt dense, avec un volume sur pied important. La lumière pénètre peu et la strate basse se développe difficilement, à moins que des clairières soient présentes. Il faut être vigilant, car des forêts vieillissantes et peu structurées peuvent être peu durables.',
  ],
  [
    'Tree_densityinf10',
    'Densité d’arbres entre 5 et 10 mètres (nombre/ha)',
    'Il s’agit du nombre de tige entre 5 et 10 mètres de hauteur par hectare. Un nombre important est synonyme de forêt dense qui laisse peu de place à la lumière. Si des clairières existent et sont bien réparties, cela peut néanmoins convenir à la Gélinotte des bois.',
  ],
  [
    'MaxVeg',
    'Hauteur maximale des arbres (m)',
    'Cette variable sert à identifier la maturité du peuplement.',
  ],
  [
    'Tree.sumV',
    'Volume total des arbres (m³/ha)',
    'Volume total présent sur pied.',
  ],
  [
    'Gha',
    'Surface terrière (m²/ha)',
    'Cette donnée montre le volume sur pied dans la parcelle. Elle confond ici la surface terrière des feuillus et des résineux.',
  ],
  [
    'prop_res',
    'Proportion de conifères (en %)',
    'La proportion feuillus/résineux doit, pour le tétras, être en faveur du résineux. Selon la productivité de la forêt, on cherche un mélange avec 15% à 20% de feuillus en nombre de tiges. La Gélinotte accepte, elle, jusqu’à 25% de mélange feuillus.',
  ],
  [
    'small_gap',
    'Proportion de trouées entre 25 et 200 m² (en %)',
    'Il s’agit ici de décrire les petites clairières (<200m²) qui offrent de la ressource alimentaire (herbacée) et un bon étagement de la végétation.',
  ],
  [
    'middle_gap',
    'Proportion de trouées entre 200 et 1000m²',
    'Il s’agit ici de décrire les clairières de taille moyenne (<1000 m²) qui offrent de la ressource alimentaire (herbacée) et un bon étagement de la végétation.',
  ],
  [
    'grassland',
    'Proportion de zones ouvertes de 1000m² minimum (en %)',
    'Les Tétraoninés cherchent des forêts en « Mosaïque ». Les zones ouvertes sont donc importantes car elles apportent la ressource alimentaire estivale et sont de bonnes zones à nichées. Elles sont donc à favoriser. Attention, pour la Gélinotte, elles ne doivent pas être trop nombreuses.',
  ],
  [
    'penetrationratio005',
    'Niveau de pénétration de la lumière entre 0 et 0,5m (en %)',
    'En décrivant la pénétration de la lumière, on décrit la densité de la végétation. Entre 0 et 0,5 mètre, on peut notamment retrouver de l’herbe, de la myrtille ou du semis de hêtre.',
  ],
  [
    'penetrationratio0501',
    'Niveau de pénétration entre 0,5 et 1m (en %)',
    'En décrivant la pénétration de la lumière, on décrit la densité de la végétation. Entre 0,5 et 1 mètre, il s’agit souvent de semis de hêtre, surtout si la pénétration est très faible.',
  ],
  [
    'penetrationratio0102',
    'Niveau de pénétration de la lumière entre 1 et 2m (en %)',
    'En décrivant la pénétration de la lumière, on décrit la densité de la végétation. Entre 1 et 2 mètres, on peut notamment retrouver des ligneux, des feuillus ou des résineux. Moins la pénétration est bonne, plus le sous-étage est fermé.',
  ],
  [
    'penetrationratio0205',
    'Niveau de pénétration de la lumière entre 2 et 5m (en %)',
    'En décrivant la pénétration de la lumière, on décrit la densité de la végétation. Entre 2 et 5 mètres, on peut notamment retrouver des ligneux, des feuillus ou des résineux. Moins la pénétration est bonne, plus le sous-étage est fermé.',
  ],
  [
    'penetrationratio0510',
    'Niveau de pénétration de la lumière entre 5 et 10m',
    'En décrivant la pénétration de la lumière, on décrit la densité de la végétation. Entre 5 et 10 mètres, on peut notamment retrouver des ligneux, des feuillus ou des résineux. Moins la pénétration est bonne, plus le sous-étage est fermé.',
  ],
  [
    'penetrationratio1020',
    'Niveau de pénétration de la lumière entre 10 et 20m (en %)',
    'Moins la pénétration est bonne et plus la forêt est fermée.',
  ],
  [
    'penetrationratio2030',
    'Niveau de pénétration de la lumière entre 20 et 30m (en %)',
    'Moins la pénétration est bonne et plus la forêt est fermée.',
  ],
  [
    'penetrationratio3060',
    'Niveau de pénétration entre 30 et 60m (en %)',
    'Moins la pénétration est bonne et plus la forêt est fermée.',
  ],
]

export default variables
