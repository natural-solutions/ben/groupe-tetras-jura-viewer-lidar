import { StepType } from '@reactour/tour/dist/types'

const steps: StepType[] = [
  {
    selector: '[data-tour="1"]',
    content: function tour1() {
      return (
        <>
          <h1>
            Bienvenue sur la plateforme de visualisation des modèles LiDAR
            &quot;Tétraoninés&quot; !
          </h1>
          <img src="/thumbnail_Montage bateau.png" width="100%" />
          <h4>Description du contenu de la plateforme :</h4>
          <p>
            Le &quot;Light Detetection And Ranging&quot; ou LiDAR est une
            technologie permettant{' '}
            <strong>de scanner la structure de la végétation</strong>
            avec une grande précision. Ici, cette technologie a été utilisée
            pour créer des cartes (aussi appelés &quot;modèles&quot;). Celles-ci
            représentent, de façon simplifiée,
            <strong>
              la probabilité de présence des Tétraoninés jurassiens
            </strong>
            , à savoir le Grand tétras et la Gélinotte des bois.
          </p>
        </>
      )
    },
  },
  {
    selector: '#deckgl-overlay',
    content: `La carte IGN vous permet de vous déplacer jusqu’à votre secteur d’intérêt (parcelle, massif, etc.).`,
  },
  {
    selector: '[data-tour="3"]',
    content: function tour3() {
      return (
        <>
          À l’aide du panneau situé ci-contre, vous pourrez choisir la carte
          LiDAR que vous souhaitez afficher. Vous avez notamment le choix dans
          l’échelle, l’espèce et le département. Note : L’échelle représente la
          surface sur laquelle est valable chaque donnée : une petite échelle
          signifie donc qu’il y a beaucoup d’informations au mètre carré,
          contrairement à une grande échelle.
        </>
      )
    },
  },
  {
    selector: '[data-tour="4"]',
    content: function tour4() {
      return (
        <>
          La poule de Grand tétras fréquente souvent des habitats différents du
          coq. Ainsi, avec certains modèles, vous aurez la possibilité d’affiner
          votre choix avec la saison et le sexe souhaités
        </>
      )
    },
  },
  {
    selector: '#deckgl-overlay',
    content: function deckglOverlay() {
      return (
        <>
          Sur la carte, vous pouvez également voir des polygones gris se
          dessiner. Il s’agit du cadastre français. Pour sélectionner les
          parcelles à examiner, cliquer sur chaque polygone faisant partie de
          votre secteur d’intérêt. En cas d’erreur, vous pouvez corriger en
          cliquant sur les petites croix présentes sur la gauche de l’écran.
          Note : Le parcellaire ONF sera disponible dans une prochaine version
          de cette plateforme.
        </>
      )
    },
  },
  {
    selector: '[data-tour="6"]',
    content: function tour6() {
      return (
        <>
          À gauche de l’écran, une liste s’affichera : elle présente les
          caractéristiques de la forêt dans le secteur sélectionné. Il s’agit
          des composantes principales de l’habitat du Grand tétras et/ou de la
          Gélinotte des bois. Pour chaque &quot;paramètre environnemental&quot;,
          vous pouvez disposer d’explications en cliquant sur le point
          d’interrogation correspondant. Les nombres affichés vous permettront
          de connaître, pour chaque paramètre, le niveau moyen* du secteur
          d’intérêt par rapport à celui du massif. Cette information peut servir
          à identifier un déséquilibre sur une des composantes, et ainsi
          encourager à la réalisation de travaux pour une meilleure prise en
          compte des Tétraoninés sur le secteur d’intérêt. À côté des nombres se
          trouvent deux cases, cochées ou non. Celles-ci distinguent les données
          utilisées pour le calcul des modèles présentés. Autrement dit, les
          variables essentielles pour chaque espèce sont cochées. (Celle de
          gauche pour la Gélinotte des bois et celle de droite pour le Grand
          tétras). Note : Une infobulle rappelle cette information lors du
          survol de la souris
        </>
      )
    },
  },
]

export default steps
