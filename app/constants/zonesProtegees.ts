export type ZoneProtegee = [string, string, string]
export type ZonesProtegees = ZoneProtegee[]
export enum ZonesProtegeesColumn {
  name = 0,
  color = 1,
  link = 2,
}

const ZONES_PROTEGEES: ZonesProtegees = [
  [
    'Arrêté Préfectoral de Protection de Biotope des forêts d’altitude',
    '#f7fbff',
    'http://www.parc-haut-jura.fr/fr/rubriques-editoriales/quietude-attitude/quelles-sont-les-zones-protegees.882-1233__3294.php',
  ],
  [
    'Réserve Naturelle Nationale de la Haute-Chaîne du Jura',
    '#b0d2e8',
    'https://www.rnn-hautechainedujura.fr/reglementation/interdits-en-rnn/',
  ],
  [
    'Zone de Quiétude pour la Faune Sauvage',
    '#3e8ec4',
    'https://www.rnn-hautechainedujura.fr/reglementation/zones-de-quietude-de-la-faune-sauvage/',
  ],
]

export default ZONES_PROTEGEES
