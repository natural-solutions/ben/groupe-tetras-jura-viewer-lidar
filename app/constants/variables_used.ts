const variable_used_gt = [
  'Gha',
  'Tree_density20',
  'small_gap',
  'Tree_density10',
  'middle_gap',
  'grassland',
  'Tree_densityinf10',
]

const variable_used_gel = [
  'Tree_density10',
  'middle_gap',
  'grassland',
  'Tree_densityinf10',
  'penetrationratio0501',
  'penetrationratio0205',
  'prop_res',
]

export default { variable_used_gel, variable_used_gt }
