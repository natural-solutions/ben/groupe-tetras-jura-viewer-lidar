import json
import glob
import os
import shutil
from typing import List
import urllib.request

CADASTRE_URL = os.environ.get(
    "CADASTRE_URL",
    "https://cadastre.data.gouv.fr/data/etalab-cadastre/latest/geojson/departements",
)
DEPARTEMENTS = os.environ.get("DEPARTEMENTS", "01,25,39")

os.makedirs("./data/tmp", exist_ok=True)


def download_clip_and_merge(
    cadastre_url: str,
    departements: List[str],
    output_filename: str = "./data/tmp/merged.geojson",
):
    if os.path.isdir("./data/cadastre"):
        os.makedirs("./data/cadastre", exist_ok=True)
    print("downloading cadastre files...")

    for departement in departements:
        urllib.request.urlretrieve(
            f"{cadastre_url}/{departement}/cadastre-{departement}-parcelles.json.gz",
            f"./data/cadastre/{departement}.json.gz",
        )

    files = glob.glob("./data/cadastre/*.json.gz")
    output = {"type": "FeatureCollection", "features": []}

    print("clipping and merging cadastre files...")

    for file in files:
        name = os.path.splitext(os.path.basename(file))[0]
        os.system(
            f"ogr2ogr -f GeoJSON ./data/tmp/{name}_clipped.geojson -clipsrc ./data/areas_presence.geojson /vsigzip/{file}"
        )
        geojson = json.load(open(f"./data/tmp/{name}_clipped.geojson"))

        for feature in geojson["features"]:
            output["features"].append(feature)

    json.dump(output, open(output_filename, "w"))


def zonal_stats(
    geojson: str = "./data/tmp/merged.geojson",
    variables_dir: str = "./data/variables",
    stats: str = "median",
):
    variables = glob.glob(f"{variables_dir}/*.tif")
    geojson_name = os.path.splitext(os.path.basename(geojson))[0]

    os.system(f"cp {geojson} ./data/tmp/{geojson_name}_stats.geojson")
    os.system(f"mv {geojson} ./data/cadastre.geojson")

    print("add zonal stats to merged file...")

    for variable in variables:
        prefix = os.path.splitext(os.path.basename(variable))[0]
        os.system(
            f'rio zonalstats ./data/tmp/{geojson_name}_stats.geojson -r {variable} --prefix {prefix} --stats "{stats}" > ./data/tmp/{geojson_name}_stats.geojson.tmp'
        )
        os.system(f"mv ./data/tmp/{geojson_name}_stats.geojson.tmp ./data/tmp/{geojson_name}_stats.geojson")
        print(f'done: {variable}')


def split_parcelles(input: str = "./data/tmp/merged_stats.geojson", parcelles_dir: str = "./data/parcelles"):
    os.makedirs("./data/parcelles", exist_ok=True)
    geojson = json.load(open(input))

    print("splitting parcelles...")

    for feature in geojson["features"]:
        json.dump(
            feature["properties"],
            open(f'{parcelles_dir}/{feature["id"]}.json', "w"),
        )


def clean():
    shutil.rmtree('./data/tmp')
    shutil.rmtree('./data/cadastre')


download_clip_and_merge(cadastre_url=CADASTRE_URL, departements=DEPARTEMENTS.split(","))
zonal_stats()
split_parcelles()
clean()
