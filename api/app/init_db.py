from fastapi_users.manager import UserAlreadyExists

from db import get_user_db
from models import UserCreate
from users import get_user_manager
from config import settings


async def create_superuser(email: str, password: str):
    try:
        for user_db in get_user_db():
            for user_manager in get_user_manager(user_db):
                superuser = await user_manager.create(
                    UserCreate(email=email, 
                               password=password, 
                               is_superuser=True, 
                               is_active=True,
                               is_verified=True)
                )
                print(f"Superuser created {superuser}")
    except UserAlreadyExists:
        print(f"Superuser {email} already exist")


async def main():
    await create_superuser(settings.FIRST_USER_EMAIL, 
                           settings.FIRST_USER_PASSWORD,)


if __name__ == '__main__':
    import asyncio
    asyncio.run(main())
