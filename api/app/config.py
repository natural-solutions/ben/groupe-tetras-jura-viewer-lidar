from pydantic import BaseSettings

class Settings(BaseSettings):
  FIRST_USER_EMAIL: str
  FIRST_USER_PASSWORD: str
  SECRET: str = "secret"
  JWT_LIFETIME: int = 3600

settings = Settings()
