from fastapi import Depends, FastAPI

from db import database, users, engine
from users import current_superuser_user, fastapi_users, jwt_authentication


app = FastAPI(openapi_url="/api/openapi.json", docs_url="/api/docs")

app.include_router(
    fastapi_users.get_auth_router(jwt_authentication), prefix="/api/auth/jwt", tags=["auth"]
)
app.include_router(
    fastapi_users.get_register_router(), prefix="/api/auth", tags=["auth"], dependencies=[Depends(current_superuser_user)]
)
app.include_router(
    fastapi_users.get_reset_password_router(),
    prefix="/api/auth",
    tags=["auth"],
)
app.include_router(
    fastapi_users.get_verify_router(),
    prefix="/api/auth",
    tags=["auth"],
)
app.include_router(fastapi_users.get_users_router(), prefix="/api/users", tags=["users"])

@app.get(
    "/api/users",
    tags=["users"],
    dependencies=[Depends(current_superuser_user)]
)
async def list_users():
    return engine.execute(users.select()).all()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()