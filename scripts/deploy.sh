#!/bin/bash

set -e # exit in case of error

export DATA_FOLDER=/home/gitlab-runner/data
export DB_FOLDER=${DATA_FOLDER}/db

ACME_FILE=acme.json

# Create acme.json if it doesn't exist
if [ ! -f "$ACME_FILE" ]; then
  touch acme.json
  chmod 600 acme.json
fi

# Login
docker login ${CI_REGISTRY} -u gitlab-ci-token -p ${CI_BUILD_TOKEN}

# Pull docker images
docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull

# Start the stack
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

# Prune 
docker image prune -a -f
docker container prune --filter "until=48h" -f
