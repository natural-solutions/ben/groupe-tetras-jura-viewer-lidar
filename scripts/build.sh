#!/bin/bash

set -e # exit in case of error

docker login ${CI_REGISTRY} -u gitlab-ci-token -p ${CI_BUILD_TOKEN}
docker pull ${CI_REGISTRY_IMAGE}:app-latest || true
docker pull ${CI_REGISTRY_IMAGE}:api-latest || true
docker pull ${CI_REGISTRY_IMAGE}:data-latest || true
docker build --cache-from ${CI_REGISTRY_IMAGE}:app-latest --tag ${CI_REGISTRY_IMAGE}:app-latest ./app
docker build --cache-from ${CI_REGISTRY_IMAGE}:api-latest --tag ${CI_REGISTRY_IMAGE}:api-latest ./api
docker build --cache-from ${CI_REGISTRY_IMAGE}:data-latest --tag ${CI_REGISTRY_IMAGE}:data-latest ./data
docker push ${CI_REGISTRY_IMAGE}:app-latest
docker push ${CI_REGISTRY_IMAGE}:api-latest
docker push ${CI_REGISTRY_IMAGE}:data-latest
